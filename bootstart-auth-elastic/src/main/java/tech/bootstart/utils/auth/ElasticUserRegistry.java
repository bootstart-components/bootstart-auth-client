package tech.bootstart.utils.auth;

import java.io.IOException;

import org.elasticsearch.action.get.GetRequestBuilder;
import org.elasticsearch.action.get.GetResponse;
import org.elasticsearch.action.search.SearchRequestBuilder;
import org.elasticsearch.action.search.SearchResponse;
import org.elasticsearch.client.Client;
import org.elasticsearch.index.query.QueryBuilders;
import org.elasticsearch.search.SearchHit;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import tech.bootstart.component.auth.UserRegistry;
import tech.bootstart.utils.data.JsonHelper;

public class ElasticUserRegistry extends UserRegistry {

	Client esClient = null;
	String registryIndex = "security";
	public String getRegistryIndex() {
		return registryIndex;
	}

	public void setRegistryIndex(String registryIndex) {
		this.registryIndex = registryIndex;
	}

	public String getRegistryAccountType() {
		return registryAccountType;
	}

	public void setRegistryAccountType(String registryAccountType) {
		this.registryAccountType = registryAccountType;
	}

	String registryAccountType = "account";

	public void setElasticClient(Client client) {
		esClient = client;
	}

	@Override
	public String create(String userId, JsonNode userNode) throws Exception {
		esClient.prepareIndex()
			.setType(registryAccountType)
			.setId(userId)
			.setIndex(registryIndex)
			.setSource(JsonHelper.asBytes(userNode))
			.execute();
		return userId;
	}

	@Override
	public void updatePassword(String userId, String password) throws Exception {
		ObjectNode updateNode = JsonHelper.mapper.createObjectNode();
		ObjectNode passwordNode = JsonHelper.mapper.createObjectNode();
		passwordNode.put("value",password);
		updateNode.set("password", passwordNode);
		esClient.prepareUpdate()
			.setType(registryAccountType)
			.setId(userId)
			.setIndex(registryIndex)
			.setDoc(JsonHelper.asBytes(updateNode))
		.execute();
	}

	@Override
	public void updateRefreshToken(String userId, String provider, String token)
			throws Exception {
		ObjectNode updateNode = JsonHelper.mapper.createObjectNode();
		{
			ObjectNode providersNode = JsonHelper.mapper.createObjectNode();
			{
				ObjectNode curProviderNode = JsonHelper.mapper.createObjectNode();
				{
					curProviderNode.put("refresh_token",token);
					providersNode.set(provider, curProviderNode);
				}
			}
			updateNode.set("providers", providersNode);
		}
		esClient.prepareUpdate()
			.setType(registryAccountType)
			.setId(userId)
			.setIndex(registryIndex)
			.setDoc(JsonHelper.asBytes(updateNode))
		.execute();

	}

	@Override
	public JsonNode retrieveByEmail(String userEmail) throws Exception {
		SearchRequestBuilder search = esClient.prepareSearch()
			.setIndices(registryIndex)
			.setTypes(registryAccountType)
			.setQuery(QueryBuilders.termQuery("emails.value", userEmail));
        SearchResponse response = search.execute().actionGet();
        SearchHit userHit = response.getHits().iterator().next();
        JsonNode userNode = null;
        if(userHit != null) {
        	userNode = JsonHelper.mapper.readTree(userHit.getSourceAsString());
        }
        return userNode;
	}

	@Override
	public JsonNode retrieveById(String userId) throws Exception {
		GetRequestBuilder request = esClient.prepareGet().setIndex(registryIndex).setType(registryAccountType).setId(userId);
		GetResponse response = request.execute().actionGet();
		JsonNode userNode = null;
		if(!response.isSourceEmpty()) {
			userNode = JsonHelper.mapper.readTree(response.getSourceAsString());
		}
		return userNode;
	}

}
