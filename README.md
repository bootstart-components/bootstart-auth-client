BootStart OAuth Client component
================================

USAGE
-----

 * General
   
   - Add MAVEN dependency to the project

```
	<dependency>
		<groupId>fr.bootstart.components</groupId>
		<artifactId>auth-client</artifactId>
		<version>1.0.0-SNAPSHOT</version>
	</dependency>

```


 * Backend REST API

   - To add auth API to app's REST API (adds a <YOUR_API_PREFIX>/account collection of endpoints), ensure fr.bootstart.component.auth.routes package is scanned by Camel.
   In a Jetty-based project, add

```
	<context-param>
Benjamin Habegger
Boot-Start || Coo-founder & CEO
Tel: +33 6 68 14 35 10
		<param-name>routeBuilder-auth</param-name>
		<param-value>packagescan:fr.bootstart.component.auth.routes</param-value>
	</context-param>
```

  - If using OAuth, add backend oauth configuration (containing confidential parts ! ) as java properties

```
auth.google.clientSecret: <GOOGLE_CLIENT_SECRET>
auth.<yourproviderid>.clientSecret: <YOUR_OAUTH_SERVER_CLIENT_SECRET>
auth.<yourproviderid>.accessTokenUrl: <YOUR_OAUTH_SERVER_TOKEN_ENDPOINT>
```

  - If using OAuth, add frontend oauth configuration information as java resource "auth/config.json"

```
{
    "google": {
        "clientId": "<GOOGLE_APP_ID>",
        "scope": [
            "https://www.googleapis.com/auth/userinfo.email",
            "https://www.googleapis.com/auth/userinfo.profile"
        ],
        "optionalUrlParams": ["access_type"],
        "accessType": "offline"
    },
    "<yourproviderid>": {
    	"clientId": "<YOUR_OAUTH_SERVER_CLIENT_ID>",
    	"url": "<OAUTH-SERVER-USER-ENDPOINT>",
    	"authorizationEndpoint": "<OAUTH-SERVER-AUTHORIZE-ENDPOINT>"
    }
}
```

  - If required (in particular, if not only OAuth is used) set beanref "auth.user.registry" to the required implementation of UserRegistry

 * Frontend AngluarJS app

   - Include javascript dependencies in your AngularJS index.html

```
	<script src="webjars/satellizer/0.12.5/satellizer.js"></script>
	<script src="auth-client/1.0.0-SNAPSHOT/bootstart.auth.js"></script>
```



   - Add the required authtication buttons to your angluarjs view
```
        <button class="btn btn-lg btn-block btn-danger text-center" ng-click="signin('google')">Connect with Google
        	<i class="fa fa-google-plus fa-inverse"></i>
        </button>
```

   - Add an angluarjs state using your view and the boostartAuthCtrl controller

```
    .state('auth', {
        url: '/',
        templateUrl: '/views/auth.html',
        controller: 'bootstartAuthCtrl',
        data: {authNotRequired: true}
    })

```

