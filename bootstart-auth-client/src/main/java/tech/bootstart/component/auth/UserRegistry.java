package tech.bootstart.component.auth;

import org.apache.camel.Exchange;
import org.apache.commons.codec.digest.DigestUtils;

import com.fasterxml.jackson.databind.JsonNode;

import java.util.Map;

public abstract class UserRegistry {
	/*
	 * User account structure
	 *
	 * {
	 * 		"emails": [
	 *     		{
	 *     			"value": "<User's email>",
	 *     			"status": ""
	 *     		}
	 *    	],
	 *    	"password": {
	 *    		"value": "<User's encrypted password>"
	 *   	}
	 *     	"roles": [
	 *     		<User's roles>
	 *		],
	 *		"providers": {
	 *         "<PROVIDER>": {
	 *         		"refresh_token": "<....>",
	 *         		"payload": {
	 *         			<PROVIDER_DATA>
	 *         		}
	 *         }
	 *      }
	 * }
	 *
	 */


	/*
	 * Storage methods
	 */

	/**
	 * @param userId	- Wanted userId (if null one should be generated)
	 * @throws Exception
	 */
    abstract public String create(String userId, JsonNode userNode) throws Exception;

    /**
     * @param userId
     * @param password
     * @throws Exception
     */
    abstract public void updatePassword(String userId, String password) throws Exception;

    /**
     * @param userId
     * @param provider
     * @param token
     * @throws Exception
     */
    abstract public void updateRefreshToken(String userId, String provider, String token) throws Exception;


    /**
     * Search for a user by email
     * @param userEmail
     * @return A user JsonNode (c.f. user structure above)
     * @throws Exception
     */
    abstract public JsonNode retrieveByEmail(String userEmail) throws Exception;

    /**
     * Search for a user by id
     * @param userId
     * @return A user JsonNode (c.f. user structure above)
     * @throws Exception
     */
    abstract public JsonNode retrieveById(String userId) throws Exception;



    String extractUserId(Exchange exchange) {
        return exchange.getIn().getHeader("user.id", String.class);
    }
    Object extractUserData(Exchange exchange) {
        return exchange.getIn().getBody();
    }
    protected String extractEmail(JsonNode node) {
        String email = null;
        if(node != null) {
            email = node.path("email").asText();
        }
        return email;
    }

	public String getDefaultRole() {
		return "user";
	}

	public String userIdFromEmail(String userEmail) {
		return DigestUtils.sha1Hex(userEmail);
	}
}
