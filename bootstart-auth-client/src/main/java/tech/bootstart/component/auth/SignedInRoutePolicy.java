package tech.bootstart.component.auth;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Route;
import org.apache.camel.spi.RoutePolicy;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.joda.time.DateTime;

import java.text.ParseException;

/**
 * Check that the user has a valid JsonToken ClaimSet
 *
 * @author benjamin
 *
 */
public class SignedInRoutePolicy implements RoutePolicy {

	@Override
	public void onInit(Route route) {}

	@Override
	public void onRemove(Route route) {}

	@Override
	public void onStart(Route route) {}

	@Override
	public void onStop(Route route) {}

	@Override
	public void onSuspend(Route route) {}

	@Override
	public void onResume(Route route) {}

	@Override
	public void onExchangeBegin(Route route, Exchange exchange) {
        Message in = exchange.getIn();
        String authHeader = in.getHeader(AuthUtils.AUTH_HEADER_KEY, String.class);

        if (StringUtils.isBlank(authHeader) || authHeader.split(" ").length != 2) {
            in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_UNAUTHORIZED);
            in.setBody(null/*Constants.AUTH_ERROR_MSG*/);
            exchange.setProperty(Exchange.ROUTE_STOP, true);
        } else {
            JWTClaimsSet claimSet = null;
            try {
                claimSet = (JWTClaimsSet) AuthUtils.decodeToken(authHeader);
            } catch (ParseException ex) {
                in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_BAD_REQUEST);
                in.setBody(null/*Constants.JWT_ERROR_MSG*/);
                exchange.setProperty(Exchange.ROUTE_STOP, true);
                return;
            } catch (JOSEException ex) {
                in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_BAD_REQUEST);
                in.setBody(null/*Constants.JWT_INVALID_MSG*/);
                exchange.setProperty(Exchange.ROUTE_STOP, true);
                return;
            }
            // ensure that the token is not expired
            if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
                in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_UNAUTHORIZED);
                in.setBody(null/*Constants.EXPIRE_ERROR_MSG*/);
                exchange.setProperty(Exchange.ROUTE_STOP, true);
            }
            exchange.setProperty("user.id", claimSet.getSubject());
        }
	}

	@Override
	public void onExchangeDone(Route route, Exchange exchange) {}

}
