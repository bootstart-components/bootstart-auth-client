package tech.bootstart.component.auth;

import org.mindrot.jbcrypt.BCrypt;

public final class PassUtils {

    public static String hashPassword(String plaintext) {
        return BCrypt.hashpw(plaintext, BCrypt.gensalt());
    }

    public static boolean checkPassword(String plaintext, String hashed) {
        return BCrypt.checkpw(plaintext, hashed);
    }

    public static void main(String[] args) {
    	for(String pass: args) {
    		System.out.println(pass+": "+PassUtils.hashPassword(pass));
    	}
    }
}
