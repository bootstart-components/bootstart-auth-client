package tech.bootstart.component.auth;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Route;
import org.apache.camel.spi.RoutePolicy;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.joda.time.DateTime;

import java.text.ParseException;

/**
 * Check that the user has the appropriate role in his JsonToken ClaimSet
 * Required roles should be set as exchange properties on the routes requiring them
 *
 * @author benjamin
 *
 */
public class PropertyBasedRoleCheckRoutePolicy implements RoutePolicy {

	@Override
	public void onInit(Route route) {}

	@Override
	public void onRemove(Route route) {}

	@Override
	public void onStart(Route route) {}

	@Override
	public void onStop(Route route) {}

	@Override
	public void onSuspend(Route route) {}

	@Override
	public void onResume(Route route) {}

	@Override
	public void onExchangeBegin(Route route, Exchange exchange) {
        Message in = exchange.getIn();
        String authHeader = in.getHeader(AuthUtils.AUTH_HEADER_KEY, String.class);

        String role = exchange.getProperty(AuthConstants.ROLE_ATTRIBUTE, String.class);
        if (role != null && !role.isEmpty() && !role.equalsIgnoreCase("none")) {
            if (StringUtils.isBlank(authHeader) || authHeader.split(" ").length != 2) {
                in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_UNAUTHORIZED);
                in.setBody(null/*Constants.AUTH_ERROR_MSG*/);
                exchange.setProperty(Exchange.ROUTE_STOP, true);
            } else {
                JWTClaimsSet claimSet = null;
                try {
                    claimSet = (JWTClaimsSet) AuthUtils.decodeToken(authHeader);
                } catch (ParseException | JOSEException ex) {
                    in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_BAD_REQUEST);
                    in.setBody(null/*Constants.JWT_ERROR_MSG*/);
                    exchange.setProperty(Exchange.ROUTE_STOP, true);
                    return;
                }
                // ensure that the token is not expired
                if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
                    in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_UNAUTHORIZED);
                    in.setBody(null/*Constants.EXPIRE_ERROR_MSG*/);
                    exchange.setProperty(Exchange.ROUTE_STOP, true);
                }
                // ensure the token gives enough rights
                String claimedRole = null;
                try {
                    claimedRole = claimSet.getStringClaim(AuthConstants.ROLE_ATTRIBUTE);
                } catch (ParseException e) {
                    e.printStackTrace();
                }
                if (claimedRole == null || claimedRole.isEmpty() || !claimedRole.equalsIgnoreCase(role)) {
                    in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_FORBIDDEN);
                    in.setBody(null);
                    exchange.setProperty(Exchange.ROUTE_STOP, true);
                }
                // setup properties indicating id/role for use in routes
                exchange.setProperty("user.id", claimSet.getSubject());
                try {
                    exchange.setProperty("user.role", claimSet.getStringClaim(AuthConstants.ROLE_ATTRIBUTE));
                } catch (ParseException e) {
                    e.printStackTrace();
                }
            }
        }
	}

	@Override
	public void onExchangeDone(Route route, Exchange exchange) {}

}
