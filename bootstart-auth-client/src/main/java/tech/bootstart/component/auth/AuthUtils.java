package tech.bootstart.component.auth;

import com.nimbusds.jose.*;
import com.nimbusds.jose.crypto.MACSigner;
import com.nimbusds.jose.crypto.MACVerifier;
import com.nimbusds.jose.crypto.RSASSAVerifier;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.joda.time.DateTime;

import java.security.KeyFactory;
import java.security.NoSuchAlgorithmException;
import java.security.interfaces.RSAPublicKey;
import java.security.spec.InvalidKeySpecException;
import java.security.spec.X509EncodedKeySpec;
import java.text.ParseException;
import java.util.Base64;

public final class AuthUtils {

    private static final JWSHeader JWT_HEADER = new JWSHeader(JWSAlgorithm.HS512);
    private static final String TOKEN_SECRET = "WJDsFNPuXLErpU1ZRDFczHFgz4IM54rOtWxwB3vOgjVJlhAFaQttSKHVMu3Bpt8WJDsFNPuXLErpU1ZRDFczHFgz4IM54rOtWxwB3vOgjVJlhAFaQttSKHVMu3Bpt8";
    public static final String AUTH_HEADER_KEY = "Authorization";

    public static String getSubject(String authHeader) throws ParseException, JOSEException {
        return decodeToken(authHeader).getSubject();
    }

    public static JWTClaimsSet decodeRSAToken(String authHeader, String publicKey) throws ParseException, JOSEException {
        SignedJWT signedJWT = SignedJWT.parse(getSerializedToken(authHeader));

        if (verifyJWTToken(signedJWT, publicKey)) {
            return signedJWT.getJWTClaimsSet();
        } else {
            throw new JOSEException("Signature verification failed");
        }
    }

    public static boolean verifyJWTToken(SignedJWT signedJWT, String publicKeyB64) {
        byte[] publicKeyBytes = Base64.getDecoder().decode(publicKeyB64);
        X509EncodedKeySpec keySpec = new X509EncodedKeySpec(publicKeyBytes);

        try {
            KeyFactory keyFactory = KeyFactory.getInstance("RSA");
            RSAPublicKey publicKey = (RSAPublicKey) keyFactory.generatePublic(keySpec);
            JWSVerifier verifier = new RSASSAVerifier(publicKey);
            return signedJWT.verify(verifier);
        }
        catch (NoSuchAlgorithmException | InvalidKeySpecException | JOSEException e) {
            return false;
        }
    }


    public static JWTClaimsSet decodeToken(String authHeader) throws ParseException, JOSEException {
        SignedJWT signedJWT = SignedJWT.parse(getSerializedToken(authHeader));
        if (signedJWT.verify(new MACVerifier(TOKEN_SECRET))) {
            return signedJWT.getJWTClaimsSet();
        } else {
            throw new JOSEException("Signature verification failed");
        }
    }

    public static String createToken(String sub, String role) throws JOSEException {
        return createToken(sub, null, role, null, null);
    }

    public static String createToken(String email, String userId, String role, String providerName, String providerAccessToken) throws JOSEException {
        JWTClaimsSet.Builder claimBuilder = new JWTClaimsSet.Builder();
        claimBuilder.subject(email)
            .issuer("")
            .issueTime(DateTime.now().toDate())
            .expirationTime(DateTime.now().plusDays(14).toDate());

        if (role != null && !role.isEmpty()) {
            claimBuilder.claim(AuthConstants.ROLE_ATTRIBUTE, role);
        }
        if (userId != null && !userId.isEmpty()) {
            claimBuilder.claim(AuthConstants.USER_ATTRIBUTE, userId);
        }
        if (providerName != null && !providerName.isEmpty()) {
            claimBuilder.claim(AuthConstants.PROVIDER_NAME_ATTRIBUTE, providerName);
        }
        if (providerAccessToken != null && !providerAccessToken.isEmpty()) {
            claimBuilder.claim(AuthConstants.PROVIDER_ACCESS_TOKEN_ATTRIBUTE, providerAccessToken);
        }

        JWTClaimsSet claim = claimBuilder.build();
        SignedJWT jwt = new SignedJWT(JWT_HEADER, claim);

        JWSSigner signer = new MACSigner(TOKEN_SECRET);
        jwt.sign(signer);

        return jwt.serialize();
    }

    public static String getSerializedToken(String authHeader) {
        return authHeader.split(" ")[1];
    }
}
