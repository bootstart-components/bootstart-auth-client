package tech.bootstart.component.auth;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Route;
import org.apache.camel.spi.RoutePolicy;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

public class PermissionCheckRoutePolicy implements RoutePolicy {

	String requiredPremission;
	public PermissionCheckRoutePolicy(String permission) {
		this.requiredPremission = permission;
	}

	@Override
	public void onInit(Route route) {}

	@Override
	public void onRemove(Route route) {}

	@Override
	public void onStart(Route route) {}

	@Override
	public void onStop(Route route) {}

	@Override
	public void onSuspend(Route route) {}

	@Override
	public void onResume(Route route) {}

	@Override
	public void onExchangeBegin(Route route, Exchange exchange) {
        Message in = exchange.getIn();
        String authHeader = in.getHeader(AuthUtils.AUTH_HEADER_KEY, String.class);

        if (StringUtils.isBlank(authHeader) || authHeader.split(" ").length != 2) {
            fail(exchange);
            return;
        } else {
            JWTClaimsSet claimSet = null;
            try {
                claimSet = (JWTClaimsSet) AuthUtils.decodeToken(authHeader);
            } catch (ParseException | JOSEException ex) {
                fail(exchange);
                return;
            }
            // ensure that the token is not expired
            if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
                fail(exchange);
                return;
            }
            // ensure the token gives enough rights
            String claimedRole = null;
            try {
                claimedRole = claimSet.getStringClaim(AuthConstants.ROLE_ATTRIBUTE);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            Collection<String> permissions = getPermissionsForRole(claimedRole);
            if (claimedRole == null || claimedRole.isEmpty() || !permissions.contains(requiredPremission)) {
                fail(exchange);
                return;
            }
        }
	}

	private Collection<String> getPermissionsForRole(String claimedRole) {
		return new ArrayList<String>(){{ add(claimedRole); }};
	}

	private static void fail(Exchange ex) {
		Message in = ex.getIn();
		in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_FORBIDDEN);
        in.setBody(null);
        ex.setProperty(Exchange.ROUTE_STOP, true);
	}

	@Override
	public void onExchangeDone(Route route, Exchange exchange) {}

}
