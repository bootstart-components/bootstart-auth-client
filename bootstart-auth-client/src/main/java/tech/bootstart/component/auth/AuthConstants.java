package tech.bootstart.component.auth;

public interface AuthConstants {
    String AUTH_ERROR_MSG = "Please make sure your request has an Authorization header";
    String EXPIRE_ERROR_MSG = "Token has expired";
    String JWT_ERROR_MSG = "Unable to parse JWT";
    String JWT_INVALID_MSG = "Invalid JWT token";
    String STATUS_MESSAGE_PROPERTY = "AuthStatusMessage";

	String PROVIDER_ID_HEADER = "provider.id";
	String PRETTY_PRINT_HEADER = "pretty";
    String BEARER = "Bearer ";
    String TOKEN_PARSED = "tokenParsed ";

	String PROVIDER_NAME_ATTRIBUTE = "provider.name";
	String PROVIDER_ACCESS_TOKEN_ATTRIBUTE = "provider.access_token";
	String PROVIDER_REFRESH_ATTRIBUTE_TOKEN = "provider.refresh_token";
    String ROLE_ATTRIBUTE = "role";
	String USER_ATTRIBUTE = "user";

    String CLIENT_ID = "clientId";
    String CLIENT_CODE = "code";
    String REDIRECT_URI = "redirectUri";
}
