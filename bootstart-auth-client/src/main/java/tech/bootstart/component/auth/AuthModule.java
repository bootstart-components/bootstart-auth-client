package tech.bootstart.component.auth;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.nimbusds.jwt.JWTClaimsSet;
import com.nimbusds.jwt.SignedJWT;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Processor;
import org.apache.camel.ProducerTemplate;
import org.apache.camel.component.http4.HttpEndpoint;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.IOException;
import java.io.Reader;
import java.util.Base64;

public class AuthModule {
    static Logger logger = LoggerFactory.getLogger(AuthModule.class);
    static ObjectMapper mapper = new ObjectMapper();

    private UserRegistry userRegistry;

    public UserRegistry getUserRegistry() {
        return userRegistry;
    }

    public void setUserRegistry(UserRegistry userRegistry) {
        this.userRegistry = userRegistry;
    }

    public AuthModule() {
    }

    public void signup(Exchange exchange) throws Exception {
        Message in = exchange.getIn();
        JsonNode user = getUnmarshalledJsonNode(in);

        String userRole = userRegistry.getDefaultRole();
        String userEmail = user.path("email").asText();
        String userPasswordHash = PassUtils.hashPassword(user.path("password").asText());
        JsonNode extra = user.path("extra");

        JsonNode foundUser = userRegistry.retrieveByEmail(userEmail);
        if (foundUser != null) {
            // Refuse existing user
            exchange.getOut().setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_CONFLICT);
            return;
        }

        String userId = exchange.getContext().getUuidGenerator().generateUuid();
        userRegistry.create(userId, user);

        String token = AuthUtils.createToken(userEmail, userRole); // Paramètre supplémentaire user.path("handle").asText()

        Message out = exchange.getOut();
        out.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_CREATED);
        out.setBody("{ \"token\": \"" + token + "\" }");
    }

    private JsonNode getUnmarshalledJsonNode(Message in) throws JsonProcessingException, IOException {
        JsonNode node = in.getBody(JsonNode.class);
        if (node == null) {
            Reader r = in.getBody(Reader.class);
            node = mapper.readTree(r);
        }
        return node;
    }

    public void signin(Exchange exchange) throws Exception {
        Message in = exchange.getIn();

        JsonNode user = getUnmarshalledJsonNode(in);
        String email = user.path("email").asText();
        String password = user.path("password").asText();

        Message out = exchange.getOut();
        JsonNode foundUser = userRegistry.retrieveByEmail(email);
        if (foundUser != null) {
            String foundUserPassword = foundUser.path("password").path("value").asText();
            if (foundUserPassword != null && PassUtils.checkPassword(password, foundUserPassword)) {
                String role = foundUser.path("roles").path(0).asText();
                String token = AuthUtils.createToken(email, role);
                out.setBody("{ \"token\": \"" + token + "\" }");
            } else {
                out.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_UNAUTHORIZED);
                out.setBody(null);
            }
        } else {
            out.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_UNAUTHORIZED);
            out.setBody(null);
        }
    }

    public void oauthAuthenticate(Exchange exchange) throws Exception {
        Message in = exchange.getIn();

        String providerId = in.getHeader(AuthConstants.PROVIDER_ID_HEADER, String.class);

        JsonNode oauthAuthorization = getUnmarshalledJsonNode(in);
        String clientId = oauthAuthorization.path("clientId").asText();
        String clientSecret = exchange.getContext().resolvePropertyPlaceholders("{{auth." + providerId + ".clientSecret}}");
        String code = oauthAuthorization.path("code").asText();
        String redirectUri = oauthAuthorization.path("redirectUri").asText();

        String baseAccessTokenUrl = in.getHeader("accessTokenUrl", String.class);
        if (baseAccessTokenUrl == null) {
            baseAccessTokenUrl = exchange.getContext().resolvePropertyPlaceholders("{{auth." + providerId + ".accessTokenUrl}}");
        }

        final String accessTokenUrl = baseAccessTokenUrl;
        final String content = "code=" + code
                + "&client_id=" + clientId
                + "&client_secret=" + clientSecret
                + "&redirect_uri=" + redirectUri
                + "&grant_type=authorization_code";

        ProducerTemplate producer = exchange.getContext().createProducerTemplate();
        HttpEndpoint httpEndpoint = exchange.getContext().getEndpoint("http4://localhost", HttpEndpoint.class);
        Exchange result = producer.request(httpEndpoint, new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                exchange.getIn().removeHeaders("*");
                exchange.getIn().setHeader(Exchange.HTTP_METHOD, "POST");
                exchange.getIn().setHeader(Exchange.HTTP_URI, accessTokenUrl);
                exchange.getIn().setHeader("Authorization", "Basic " + Base64.getEncoder().encodeToString((clientId + ":" + clientSecret).getBytes()));
                exchange.getIn().setHeader("Content-Length",content.getBytes().length);
                exchange.getIn().setHeader("Content-Type","application/x-www-form-urlencoded");
                exchange.getIn().setBody(content); // Ensure body is empty and not "null"
            }
        });
        Message resultOut = result.getOut();
        String resultBody = resultOut.getBody(String.class);
        if(result.isFailed()) {
            logger.error("Couldn't authenticate on "+accessTokenUrl+ " with content "+content, result.getException());
        } else if(resultBody == null) {
            logger.error("Couldn't authenticate on "+accessTokenUrl+ " with content "+content+" Status:"+resultOut.getHeader(Exchange.HTTP_RESPONSE_CODE)+" "+resultOut.getHeader(Exchange.HTTP_RESPONSE_TEXT));
            in.setBody(null);
            in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_UNAUTHORIZED);
        } else {
            ObjectMapper mapper = new ObjectMapper();
            JsonNode resultUser = mapper.readTree(resultBody);

            String userEmail;
            String providerIdToken = resultUser.path("id_token").asText();
            if (!providerIdToken.isEmpty()) {
                SignedJWT signedJWT = SignedJWT.parse(providerIdToken);
                JWTClaimsSet claimSet = signedJWT.getJWTClaimsSet();
                userEmail = claimSet.getClaim("email").toString();
            } else {
                userEmail = resultUser.path("username").asText(); // Suppose email is stored as username
            }
            String userId = userRegistry.userIdFromEmail(userEmail);
            String providerAccessToken = resultUser.path("access_token").asText();
            String providerRefreshToken = resultUser.path("refresh_token").asText();

            // userRegistry.upsert(userId, userEmail);

            if (providerRefreshToken != null) {
                userRegistry.updateRefreshToken(userId, providerId, providerRefreshToken);
            }

            String jwtSessionToken = AuthUtils.createToken(userEmail, userId, "user", providerId, providerAccessToken);

            in.setBody("{\"token\":\"" + jwtSessionToken + "\"}");
        }
    }

    public void openIDAuthenticate(Exchange exchange) throws Exception {
        Message in = exchange.getIn();

        String providerId = in.getHeader(AuthConstants.PROVIDER_ID_HEADER, String.class);

        JsonNode oauthAuthorization = getUnmarshalledJsonNode(in);
        String clientId = oauthAuthorization.path("clientId").asText();
        String clientSecret = exchange.getContext().resolvePropertyPlaceholders("{{auth." + providerId + "." + clientId + ".clientSecret}}");
        String code = oauthAuthorization.path("code").asText();
        String redirectUri = oauthAuthorization.path("redirectUri").asText();

        String baseAccessTokenUrl = in.getHeader("accessTokenUrl", String.class);
        if (baseAccessTokenUrl == null) {
            baseAccessTokenUrl = exchange.getContext().resolvePropertyPlaceholders("{{auth." + providerId + ".accessTokenUrl}}");
        }

        final String accessTokenUrl = baseAccessTokenUrl;

        ProducerTemplate producer = exchange.getContext().createProducerTemplate();
        HttpEndpoint httpEndpoint = exchange.getContext().getEndpoint("http4://localhost", HttpEndpoint.class);
        Exchange result = producer.request(httpEndpoint, new Processor() {
            @Override
            public void process(Exchange exchange) throws Exception {
                exchange.getIn().removeHeaders("*");
                exchange.getIn().setHeader(Exchange.HTTP_METHOD, "POST");
                exchange.getIn().setHeader(Exchange.HTTP_URI, accessTokenUrl);
                exchange.getIn().setHeader("Authorization", "Basic " + Base64.getEncoder().encodeToString((clientId + ":" + clientSecret).getBytes()));

                String content = "code=" + code
                    + "&client_id=" + clientId
                    + "&client_secret=" + clientSecret
                    + "&redirect_uri=" + redirectUri
                    + "&grant_type=authorization_code";
                exchange.getIn().setHeader("Content-Length",content.getBytes().length);
                exchange.getIn().setHeader("Content-Type","application/x-www-form-urlencoded");
                exchange.getIn().setBody(content); // Ensure body is empty and not "null"
            }
        });
        Message resultOut = result.getOut();
        String resultBody = resultOut.getBody(String.class);
        ObjectMapper mapper = new ObjectMapper();
        JsonNode resultNode = mapper.readTree(resultBody);

        String providerAccessToken = resultNode.path("access_token").asText();

        in.setBody("{\"token\":\"" + providerAccessToken + "\"}");
    }
}
