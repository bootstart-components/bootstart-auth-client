package tech.bootstart.component.auth.routes.core;

import tech.bootstart.component.auth.AuthModule;
import tech.bootstart.component.auth.UserRegistry;
import tech.bootstart.component.auth.example.DummyUserRegistry;
import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;
import org.apache.camel.util.CamelContextHelper;
import org.apache.http.HttpStatus;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * Define REST services using the Camel REST DSL
 */
public class AccountCoreRouteBuilder extends RouteBuilder {

    static Logger logger = LoggerFactory.getLogger(AccountCoreRouteBuilder.class);

    AuthModule auth = new AuthModule();

    @Override
    public void configure() throws Exception {
        UserRegistry userRegistry = CamelContextHelper.lookup(getContext(), "auth.user.registry", UserRegistry.class);
        if (userRegistry == null) {
            logger.error("No user registry defined (couldn't find bean under reference 'auth.user.registry'): falling back to dummy");
            userRegistry = new DummyUserRegistry();
        }
        auth.setUserRegistry(userRegistry);

        from("direct:auth.account.signup").id("auth.account.signup")
            .to("log:auth.account.signup")
            .bean(auth, "signup")
        ;

        from("direct:auth.account.signin").id("auth.account.signin")
            .to("log:auth.account.signin")
            .bean(auth, "signin")
        ;

        from("direct:auth.account.signin.oauth").id("auth.account.signin.oauth")
            .to("log:auth.account.signin.oauth")
            .filter(body().isNotNull())
                .choice()
                    .when(header("provider.id").isEqualTo("facebook"))
                        .setHeader("accessTokenUrl").constant("https://graph.facebook.com/v2.3/oauth/access_token")
                        .bean(auth, "oauthAuthenticate")
                    .when(header("provider.id").isEqualTo("google"))
                        .setHeader("accessTokenUrl").constant("https://www.googleapis.com/oauth2/v3/token")
                        .bean(auth, "oauthAuthenticate")
                    .otherwise()
                        .bean(auth, "oauthAuthenticate")
                .endChoice()
            .end()
            .removeHeader("accessTokenUrl")
            .filter(body().isNull())
                .setHeader(Exchange.HTTP_RESPONSE_CODE, constant(HttpStatus.SC_UNAUTHORIZED))
                .to("freemarker://messages/status/unauthorized.ftl.json")
            .end()
        ;
    }

}
