package tech.bootstart.component.auth;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import net.minidev.json.JSONArray;
import net.minidev.json.JSONObject;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Route;
import org.apache.camel.spi.RoutePolicy;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by stagiaire on 21/06/16.
 */
public class RBACRoutePolicy implements RoutePolicy {

    String[] rules;
    String providerId;
    public RBACRoutePolicy(String pattern, String providerId) {
        this.rules = pattern.split("\\|");;
        this.providerId = providerId;
    }

    private boolean checkRule(JWTClaimsSet claimSet, String clientId, String rule) {
        //Create the role if it is dynamic
        Pattern p = Pattern.compile("\\{(.*)\\}");
        Matcher m = p.matcher(rule);
        StringBuffer sb = new StringBuffer();
        while (m.find()) {
            if (claimSet.getClaim(m.group(1)) == null) {
                return false;
            }
            m.appendReplacement(sb, (String)claimSet.getClaim(m.group(1)));
        }
        m.appendTail(sb);
        String roleToCheck = sb.toString();

        // ensure the token gives enough rights
        try {
            JSONObject realm_access = (JSONObject)claimSet.getClaim("realm_access");
            JSONArray realmRoles = (JSONArray)realm_access.get("roles");
            if (!realmRoles.contains(roleToCheck)) {
                JSONObject resource_access = (JSONObject)claimSet.getClaim("resource_access");
                resource_access = (JSONObject)resource_access.get(clientId);
                JSONArray ressourceRoles = (JSONArray)resource_access.get("roles");
                if (!ressourceRoles.contains(roleToCheck)) {
                    return false;
                }
            }
        }
        catch (Exception e) {
            return false;
        }
        return true;
    }

    @Override
    public void onInit(Route route) {}

    @Override
    public void onRemove(Route route) {}

    @Override
    public void onStart(Route route) {}

    @Override
    public void onStop(Route route) {}

    @Override
    public void onSuspend(Route route) {}

    @Override
    public void onResume(Route route) {}

    @Override
    public void onExchangeBegin(Route route, Exchange exchange) {
        Message in = exchange.getIn();

        String clientId = in.getHeader("Client-Id", "", String.class);
        String publicKey = null;
        try {
            publicKey = exchange.getContext().resolvePropertyPlaceholders("{{auth." + providerId + ".publicKey}}");
        }
        catch (Exception e) {
            fail(exchange);
            return;
        }
        String authHeader = in.getHeader(AuthUtils.AUTH_HEADER_KEY, String.class);

        if (StringUtils.isBlank(authHeader) || authHeader.split(" ").length != 2) {
            fail(exchange);
            return;
        } else {
            JWTClaimsSet claimSet = null;
            try {
                claimSet = (JWTClaimsSet) AuthUtils.decodeRSAToken(authHeader, publicKey);
            } catch (ParseException | JOSEException ex) {
                fail(exchange);
                return;
            }
            // ensure that the token is not expired
            if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
                fail(exchange);
                return;
            }

            boolean result = false;
            for (String rule : rules) {
                if (checkRule(claimSet, clientId, rule)) {
                    result = true;
                    break;
                }
            }

            if (!result) {
                fail(exchange);
                return;
            }
            exchange.setProperty(AuthConstants.TOKEN_PARSED, claimSet);
        }
    }

    private Collection<String> getPermissionsForRole(String claimedRole) {
        return new ArrayList<String>(){{ add(claimedRole); }};
    }

    private static void fail(Exchange ex) {
        Message in = ex.getIn();
        in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_FORBIDDEN);
        in.setBody(null);
        ex.setProperty(Exchange.ROUTE_STOP, true);
    }

    @Override
    public void onExchangeDone(Route route, Exchange exchange) {}

}
