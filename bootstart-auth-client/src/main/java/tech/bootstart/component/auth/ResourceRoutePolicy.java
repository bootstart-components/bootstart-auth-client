package tech.bootstart.component.auth;

import com.nimbusds.jose.JOSEException;
import com.nimbusds.jwt.JWTClaimsSet;
import org.apache.camel.Exchange;
import org.apache.camel.Message;
import org.apache.camel.Route;
import org.apache.camel.spi.RoutePolicy;
import org.apache.commons.lang3.StringUtils;
import org.apache.http.HttpStatus;
import org.joda.time.DateTime;

import java.text.ParseException;
import java.util.ArrayList;
import java.util.Collection;

public class ResourceRoutePolicy implements RoutePolicy {

    String headerField;
    String tokenField;
    String providerId;
    public ResourceRoutePolicy(String headerField, String tokenField, String providerId) {
        this.tokenField = tokenField;
        this.headerField = headerField;
        this.providerId = providerId;
    }

    @Override
    public void onInit(Route route) {}

    @Override
    public void onRemove(Route route) {}

    @Override
    public void onStart(Route route) {}

    @Override
    public void onStop(Route route) {}

    @Override
    public void onSuspend(Route route) {}

    @Override
    public void onResume(Route route) {}

    @Override
    public void onExchangeBegin(Route route, Exchange exchange) {
        Message in = exchange.getIn();

        JWTClaimsSet claimSet = exchange.getProperty(AuthConstants.TOKEN_PARSED, JWTClaimsSet.class);

        if (claimSet == null) { //If we have to check the token, because the route has no other policy...

            String clientId = in.getHeader("Client-Id", "", String.class);
            String publicKey = null;
            try {
                publicKey = exchange.getContext().resolvePropertyPlaceholders("{{auth." + providerId + ".publicKey}}");
            }
            catch (Exception e) {
                fail(exchange);
                return;
            }
            String authHeader = in.getHeader(AuthUtils.AUTH_HEADER_KEY, String.class);

            if (StringUtils.isBlank(authHeader) || authHeader.split(" ").length != 2) {
                fail(exchange);
                return;
            } else {
                try {
                    claimSet = (JWTClaimsSet) AuthUtils.decodeRSAToken(authHeader, publicKey);
                } catch (ParseException | JOSEException ex) {
                    fail(exchange);
                    return;
                }
                // ensure that the token is not expired
                if (new DateTime(claimSet.getExpirationTime()).isBefore(DateTime.now())) {
                    fail(exchange);
                    return;
                }
            }
        }
        String headerId = in.getHeader(headerField, String.class);
        String tokenId = (String)claimSet.getClaim(tokenField);
        if (headerId == null || tokenId == null || !headerId.equals(tokenId)){
            fail(exchange);
            return;
        }
        exchange.setProperty(AuthConstants.TOKEN_PARSED, claimSet);
    }

    private Collection<String> getPermissionsForRole(String claimedRole) {
        return new ArrayList<String>(){{ add(claimedRole); }};
    }

    private static void fail(Exchange ex) {
        Message in = ex.getIn();
        in.setHeader(Exchange.HTTP_RESPONSE_CODE, HttpStatus.SC_FORBIDDEN);
        in.setBody(null);
        ex.setProperty(Exchange.ROUTE_STOP, true);
    }

    @Override
    public void onExchangeDone(Route route, Exchange exchange) {}

}
