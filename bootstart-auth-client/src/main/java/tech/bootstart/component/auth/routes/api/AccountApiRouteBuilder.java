package tech.bootstart.component.auth.routes.api;

import org.apache.camel.Exchange;
import org.apache.camel.builder.RouteBuilder;

/**
 * Define REST services using the Camel REST DSL
 */
public class AccountApiRouteBuilder extends RouteBuilder {

    @Override
    public void configure() throws Exception {
        rest("/account")
            .description("Accounts REST Services")
            .consumes("application/json").produces("application/json")

            .get("/config")
            .id("api.account.config").description("Retrieve authentication config information (available providers and their OAuth2 config)")
            .route()
                .to("freemarker://auth/config.json")
                .setHeader(Exchange.CONTENT_TYPE, constant("application/json"))
            .endRest()

            .post("/signup")
            .id("api.account.signup").description("Signup account")
            .to("direct:auth.account.signup")

            .post("/signup/{user.id}")
            .id("api.account.signup.id").description("Signup account with id in url")
            .to("direct:auth.account.signup")

            .post("/signin")
            .id("api.account.signin").description("Signin account")
            .to("direct:auth.account.signin")

            .post("/signin/oauth/{provider.id}")
            .id("api.account.signin.oauth").description("Signin with OAuth")
            .to("direct:auth.account.signin.oauth")

            .post("/signin/facebook")
            .id("api.account.signin.facebook").description("Signin with facebook")
            .to("direct:auth.account.signin.facebook")
        ;
    }

}
