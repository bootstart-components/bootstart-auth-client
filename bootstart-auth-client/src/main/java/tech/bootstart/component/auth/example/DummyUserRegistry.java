package tech.bootstart.component.auth.example;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.ObjectNode;

import tech.bootstart.component.auth.UserRegistry;

import java.util.HashMap;
import java.util.Map;

public class DummyUserRegistry extends UserRegistry {
    Map<String,JsonNode> id2User = new HashMap<>();
    Map<String,String> email2id = new HashMap<>();
	ObjectMapper mapper = new ObjectMapper();


	@Override
	public String create(String userId, JsonNode userData) throws Exception {
        String userEmail = this.extractEmail(userData);
        email2id.put(userEmail,userId);
        id2User.put(userId, userData);
		return userId;
	}

    @Override
    public JsonNode retrieveByEmail(String email) {
        String userId = email2id.get(email);
        return id2User.get(userId);
    }

	@Override
	public JsonNode retrieveById(String userId) {
        return id2User.get(userId);
	}

	@Override
	public void updatePassword(String userId, String password) throws Exception {
		ObjectNode passwordNode = (ObjectNode)id2User.get(userId).path("password");
		passwordNode.put("value", password);
	}

	@Override
	public void updateRefreshToken(String userId, String provider, String token) throws Exception {
		JsonNode userNode = id2User.get(userId);
		if(userNode != null) {
			ObjectNode providerNode = (ObjectNode) userNode.path("providers").path(provider);
			providerNode.put("refresh_token", token);
		}
	}
}
