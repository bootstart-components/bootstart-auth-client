var bootstartAuthModule = angular.module('bootstart.auth', ['satellizer'])
    .config(['$authProvider', function($authProvider) {
        $authProvider.baseUrl ='/api/v1';
        $authProvider.signupRedirect = '/';
        $authProvider.loginUrl = '/account/signin';
        $authProvider.signupUrl = '/account/signup';

        $.get('/api/v1/account/config', function(authConfig) {
        	angular.forEach(authConfig,function(providerConfig, provider){
	        	providerConfig.url = '/account/signin/oauth/'+provider;
	            providerConfig.redirectUri = window.location.origin || window.location.protocol  + '//' + window.location.host;
	        	var conf = undefined;
	        	if($authProvider[provider]) {
	        		conf = $authProvider[provider](providerConfig);
	        	} else {
	        		providerConfig['name'] = providerConfig['name'] || provider;
	        		conf = $authProvider.oauth2(providerConfig);
	        	}
        	});
        });
    }]);
    
bootstartAuthModule.controller('bootstartAuthCtrl', ['$scope', '$rootScope', '$filter', '$state','$auth', '$location','$resource',
        function($scope, $rootScope, $filter, $state, $auth, $location,$resource) {
		var signedinState = $rootScope['signedinState'] || "connect";
		$scope.extra = {};
        $scope.signin = function(provider) {
        	var successCallback = function () {
                if ($auth.isAuthenticated()) {
                    console.log("Authentication successful")
                    $state.go(signedinState);
                }
            };
            var failureCallback = function (response) {
                console.log("Authentication failed")
            };
            
            if ($auth.isAuthenticated()) {
                console.log("Already signed in")
                $state.go(signedinState);
            } else {
            	if(!provider) {
            		$auth.login({
                        email: $scope.email,
                        password: $scope.password
                    })
                    	.then(successCallback)
                    	.catch(failureCallback);
            	} else {
	                $auth.authenticate(provider)
	                    .then(successCallback)
	                    .catch(failureCallback);
            	}
            }
        };

        $scope.signup = function() {
        	var signupData = {
    			email: $scope.email,
    			password: $scope.password
        	};
        	if($scope.extra) {
        		signupData.extra = $scope.extra;
        	}
        	$auth.signup(signupData)
        		.then(function(response) {
        			$auth.setToken(response);
                    	$location.path('/');
                  	})
                  .catch(function(response) {
                    console.log("Error: "+response.data.message);
                  });
        }
        
        $scope.signout = function(){
            if($auth.isAuthenticated()){
                $auth.logout();
            }
            console.log("Disconnected");
        	$location.path('/');
        }
        
        $scope.isAuthenticated = $auth.isAuthenticated;        
    }])
;
